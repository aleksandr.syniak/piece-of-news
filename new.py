from flask import Flask, render_template, url_for, g, request, redirect, flash, Response
from flask_sqlalchemy import SQLAlchemy
from flask_login import login_user, login_required, logout_user, current_user, UserMixin, LoginManager
from werkzeug.security import generate_password_hash, check_password_hash
from werkzeug.utils import secure_filename
from datetime import datetime
from logging import debug
from werkzeug.utils import redirect
from flask_migrate import Migrate
from config import Config
import os


'''Конфигурация'''
app = Flask(__name__)
app.config.from_object(Config)
db = SQLAlchemy(app)
migrate = Migrate(app, db)
manager = LoginManager(app)

'''ТАБЛИЦЫ'''
class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(120), index=True, unique=True)
    email = db.Column(db.String(120), index=True, unique=True)
    password_hash = db.Column(db.String(255))
    posts = db.relationship('Post', backref='user', lazy='dynamic')


class Post(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(300), index=True, unique=False)
    summary = db.Column(db.Text, index=True, unique=True)
    body = db.Column(db.Text, index=True, unique=True)
    type = db.Column(db.Text, index=True,)
    images = db.Column(db.LargeBinary)
    name = db.Column(db.Text, nullable=False)
    mimetype = db.Column(db.Text, nullable=False)
    timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))


class Img(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    images = db.Column(db.LargeBinary)
    name = db.Column(db.Text, nullable=False)
    mimetype = db.Column(db.Text, nullable=False)


@manager.user_loader
def load_user(user_id):
    return User.query.get(user_id)

'''Главная'''
@app.route('/')
def index():

    post1 = Post.query.order_by(Post.timestamp.desc()).all()


    return render_template("index.html", post=post1)

'''Авторизация'''
@app.route('/Login', methods=['POST', 'GET'])
def verification():
    if request.method == 'POST':
        username = request.form.get('username')
        password = request.form.get('password')

        user = User.query.filter_by(username=username).first()

        if user and check_password_hash(user.password_hash, password):
            login_user(user)

            return redirect(url_for('index'))

        else:
            flash('не верный логин/пароль', category='error')

    return render_template("verification.html")

'''Регистрация'''
@app.route('/Registering', methods=['POST', 'GET'])
def registr():
    if request.method == 'POST':
        username = request.form['username']
        email = request.form['email']
        password = request.form['password']
        password2 = request.form['password2']
        password_hash = generate_password_hash(request.form['password'])

        user = User(username=username, email=email, password_hash=password_hash)

        try:
            db.session.add(user)
            db.session.commit()
            return redirect(url_for('index'))

        except:
            if not (username or email or password or password2):
                flash('Ошибка отправки', category='error')

            elif password != password2:
                flash('Пароли не совпадают', category='error')

    return render_template("registr.html")

'''Создание статьи'''
@app.route('/create_post', methods=['POST', 'GET'])
@login_required
def create_post():
    if request.method == 'POST':
        title = request.form['title']
        body = request.form['body']
        images = request.files['images']
        type = request.form['select']

        filename = secure_filename(images.filename)
        mimetype = images.mimetype

        try:
            post = Post(title=title, body=body,images=images.read(), name=filename, mimetype=mimetype, type=type )

            db.session.add(post)
            db.session.commit()
            db.session.rollback()
            return redirect(url_for('index'))

        except:

            flash('Ошибка отправки', category='error')
            return render_template("create_post.html")

    return render_template('create_post.html')

'''Дитализация поста'''
@app.route('/posts/<int:id>')
def post_detail(id):
    post = Post.query.get(id)

    return render_template("post_detail.html", post=post)

'''Удаление поста'''
@app.route('/posts/<int:id>/del')
def post_delete(id):
    post = Post.query.get_or_404(id)

    try:
        db.session.delete(post)
        db.session.commit()
        return redirect (url_for('index'))
    except:
        return "При удалении статьи произошла ошибка!!!"

'''Редактирование статьи'''
@app.route('/posts/<int:id>/update', methods=[ 'POST', 'GET' ])
def post_update(id):
    post = Post.query.get(id)
    if request.method == "POST":
        post.title = request.form['title']
        post.body = request.form['body']
        post.type = request.form['select']

        try:
            db.session.commit()
            return redirect (url_for('index'))
        except:
            return "При редактировании статьи произошла ошибка!!!"
    else:
        return render_template("post_update.html", post=post)

'''Спорт'''
@app.route('/sport')
def sport():

    post = Post.query.order_by(Post.timestamp.desc()).all()

    return render_template("sport.html", post=post)

'''Наука'''
@app.route('/science')
def science():

    post = Post.query.order_by(Post.timestamp.desc()).all()

    return render_template("science.html", post=post)

'''Програмирование'''
@app.route('/programming')
def programming():

    post = Post.query.order_by(Post.timestamp.desc()).all()

    return render_template("programming.html", post=post)

'''Кулинария'''
@app.route('/cooking')
def cooking():

    post = Post.query.order_by(Post.timestamp.desc()).all()

    return render_template("cooking.html", post=post)

'''Психология'''
@app.route('/psychology')
def psychology():

    post = Post.query.order_by(Post.timestamp.desc()).all()

    return render_template("psychology.html", post=post)

'''Вывидение изображений'''
@app.route('/posts/img/<int:id>')
def get_img1(id):
    img = Post.query.filter_by(id=id).first()
    if not img:
        return 'Img Not Found!', 404

    return Response(img.images, mimetype=img.mimetype)

@app.route('/<int:id>')
def get_img(id):
    img = Post.query.filter_by(id=id).first()
    if not img:
        return 'Img Not Found!', 404

    return Response(img.images, mimetype=img.mimetype)

'''Страница 404'''
@app.errorhandler(404)
def pageNotFount(erroe):
    return render_template('page404.html', title="Страница не найдена"), 404

'''Перессылка'''
@app.after_request
def redirect_to_signin(response):
    if response.status_code == 401:

        return redirect(url_for('verification'))

    return response

'''Выход из аккаунта'''
@app.route('/logout', methods=['GET', 'POST'])
@login_required
def logout():
    logout_user()
    return redirect(url_for('index'))


if __name__ == "__main__":
    app.run(debug = True)

