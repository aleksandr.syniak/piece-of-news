import os
basedir = os.path.abspath(os.path.dirname(__file__))

'''Клас конфигурации приложения'''
class Config(object):
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'kjsdklajdk43ljakfhgncvb34nvcxj'
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or \
        'sqlite:///' + os.path.join(basedir, 'base.db')
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg', 'gif'}
    UPLOAD_FOLDER = 'static\images'